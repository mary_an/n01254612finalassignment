﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_trial
{
    public partial class CreateNewPage : System.Web.UI.Page
    {
        private string insertquery = "INSERT INTO MyPages" +
           "(page_title,page_content,page_author_lName,page_author_fName,publish_date)" + "VALUES ";
        

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void AddPage(object sender, EventArgs e)
        {
            string pageName = page_name.Text.ToString();
            string pageContent = page_content.Text.ToString();
            string authorlName = author_lname.Text.ToString();
            string authorfName = author_fname.Text.ToString();
            string publishDate = DateTime.Today.ToString("dd/MM/yyyy");

            insertquery += "('" + pageName + "', '" + pageContent + "', '" +
               authorlName + "','" + authorfName + "',convert(DATETIME,'" + publishDate + "',103))";
            //insertquery += "(" + "(select max(page_id) from MyPages)+1" + ", '" + pageName + "', '" + pageContent + "','" + authorlName + "','" + authorfName + "')";
           //datetime 103 format doesn't work
            // test.InnerHtml = insertquery;
            pages_select.InsertCommand = insertquery;
            pages_select.Insert();
        }
    }
}
//the code contains adopted and modified parts from inclass example 
