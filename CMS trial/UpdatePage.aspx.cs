﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_trial
{
    public partial class UpdatePage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["page_id"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
           
            DataRowView pagerow = getPageInfo(pageid);
            if (pagerow == null)
            {
                page_name.InnerHtml = "No Page Found.";
                return;
            }
            page_title.Text = pagerow["page_title"].ToString();
            author_lname.Text = pagerow["page_author_lName"].ToString();
            author_fname.Text = pagerow["page_author_fName"].ToString();
            if (!Page.IsPostBack)
            {
                page_name.InnerHtml = page_title.Text ;

            }
            page_content.Text = pagerow["page_content"].ToString();

        }
        protected void Update_Page(object sender, EventArgs e)
        {
            string ptitle = page_title.Text;
            string lname = author_lname.Text;
            string fname = author_fname.Text;
            string content = page_content.Text;

            string updatequery = "Update MyPages set page_title='" + ptitle + "'," +
                "page_author_lName='" + lname + "',page_author_fName='" + fname + "'," +
                "page_content='" + content + "' where page_id=" + pageid;
           // test.InnerHtml = updatequery;
            update_page.UpdateCommand = updatequery;
            update_page.Update();

        }
        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from MyPages where page_id=" + pageid.ToString();
            page_select.SelectCommand = query;
            
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }

    }
}

//the code contains adopted and modified parts from inclass example 