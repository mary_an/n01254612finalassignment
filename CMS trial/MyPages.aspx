﻿<%@ Page Title="My Pages" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyPages.aspx.cs" Inherits="CMS_trial.MyPages" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:PageBox ID="pages_menu" runat="server" SkinID="DataGrid"></asp:PageBox>
    <asp:SqlDataSource runat="server"
        id="pages_select"
        ConnectionString="<%$ ConnectionStrings:Pages_connection%>">
    </asp:SqlDataSource>
    <div id="pageGrid">
        <asp:DataGrid id="pages_list" runat="server">
        </asp:DataGrid>
    </div>
    
</asp:Content>
