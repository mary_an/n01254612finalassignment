﻿<%@ Page Title="Create New Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateNewPage.aspx.cs" Inherits="CMS_trial.CreateNewPage" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <asp:PageBox ID="pages_menu" runat="server" SkinID="DataGrid"></asp:PageBox>
    <asp:SqlDataSource runat="server"
        id="pages_select"
        ConnectionString="<%$ ConnectionStrings:Pages_connection%>">
    </asp:SqlDataSource>
    <h1>Create a new page</h1>
    <div class="pageInputBox">
     <label>Page Title:</label>
        <asp:TextBox ID="page_name" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_name"
            ErrorMessage="Please enter a page name">
        </asp:RequiredFieldValidator>
    </div>
     
    <div class="pageInputBoxA">
     <label>Author's Last Name:</label>
        <asp:TextBox ID="author_lname" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="author_lname"
            ErrorMessage="Please enter your last name">
        </asp:RequiredFieldValidator>
    </div>
    <div class="pageInputBoxA">
     <label>Author's First Name:</label>
        <asp:TextBox ID="author_fname" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="author_fname"
            ErrorMessage="Please enter your first name">
        </asp:RequiredFieldValidator>
    </div>
    <div class="pageInputBox">
     <label>Main content:</label>
        <asp:TextBox ID="page_content" runat="server"  TextMode="multiline" ></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_content"
            ErrorMessage="Please create a content">
        </asp:RequiredFieldValidator>
    </div> 
     <ASP:Button ID="addPageBtn" Text="Add Page" runat="server" OnClick="AddPage"/>

   <!-- <div id="test" runat="server"></div>-->
   
</asp:Content>