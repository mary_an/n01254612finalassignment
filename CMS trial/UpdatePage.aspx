﻿<%@ Page Title="UpdatePage" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdatePage.aspx.cs" Inherits="CMS_trial.UpdatePage" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server"
        id="page_select"
        ConnectionString="<%$ ConnectionStrings:Pages_connection%>">
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="update_page"
        ConnectionString="<%$ ConnectionStrings:Pages_connection %>">
    </asp:SqlDataSource>
    <h1 runat="server" id="page_name">Update the page</h1>
    
    <div class="pageInputBox">
     <label>Page Name:</label>
        <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
    </div>
     
    <div class="pageInputBoxA">
     <label>Author's Last Name:</label>
        <asp:TextBox ID="author_lname" runat="server"></asp:TextBox>
    </div>
    <div class="pageInputBoxA">
     <label>Author's First Name:</label>
        <asp:TextBox ID="author_fname" runat="server"></asp:TextBox>
    </div>
    <div class="pageInputBox">
     <label>Main content:</label>
        <asp:TextBox ID="page_content" runat="server" TextMode="multiline"></asp:TextBox>
    </div>
     <asp:Button Text="Update Page" runat="server" OnClick="Update_Page"/>

    <!--<div id="test" runat="server"></div>-->
   
</asp:Content>