﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_trial.usercontrols
{
    public partial class PageBox : System.Web.UI.UserControl
    {
        private string queryline = "SELECT page_id," +
            "page_title as Title from MyPages";

        private string menu_display = "<ul>";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages_list_menu.SelectCommand = queryline;
            PagesList_Display(pages_list_menu);
                
        }
        protected void PagesList_Display(SqlDataSource src)
        {

            DataTable mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            foreach (DataRow row in mytbl.Rows)
            {
                menu_display += "<li><a href=Page.aspx?page_id=" + row["page_id"] + ">" + row["Title"] + "</a></li>";
            }

            menu_display += "</ul>";
            pages_display.InnerHtml = menu_display;
        }
       
    }
}
//the code contains adopted and modified parts from inclass example 