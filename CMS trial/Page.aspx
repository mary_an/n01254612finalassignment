﻿<%@ Page Title="Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="CMS_trial.Page" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server"
        id="page1_select"
        ConnectionString="<%$ ConnectionStrings:Pages_connection%>">
    </asp:SqlDataSource>
    
    <h1 id="main_h" runat="server"></h1>
    <h4>Written by <span id="main_author" runat="server"></span></h4>
    <p id="main_cont" runat="server"></p>
    <asp:SqlDataSource 
        runat="server"
        id="del_page"
        ConnectionString="<%$ ConnectionStrings:Pages_connection %>">
    </asp:SqlDataSource>
    <asp:Button runat="server" id="del_page_btn"
        OnClick="DeletePage"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" />
     
     <a href="UpdatePage.aspx?page_id=<%Response.Write(this.pageid);%>">Edit</a>

<!--the code contains adopted and modified parts from inclass example -->
</asp:Content>
