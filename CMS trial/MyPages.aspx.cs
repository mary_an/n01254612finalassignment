﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_trial
{
    public partial class MyPages : System.Web.UI.Page
    {

        private string queryline = "SELECT page_id, page_title AS Title, CONCAT(page_author_fName, ' ', page_author_lName) as Author, publish_date AS Published FROM MyPages";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = queryline;
            pages_list.DataSource = Pages_Bind(pages_select);
            pages_list.DataBind();
        }
        protected DataView Pages_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;


            foreach (DataRow row in mytbl.Rows)
            {

                row["TITLE"] =
                    "<a href=\"Page.aspx?page_id="
                    + row["page_id"]
                    + "\">"
                    + row["TITLE"]
                    + "</a>";

            }
            mytbl.Columns.Remove("page_id");
            myview = mytbl.DefaultView;

            return myview;
        }
    }
}
//the code contains adopted and modified parts from inclass example 