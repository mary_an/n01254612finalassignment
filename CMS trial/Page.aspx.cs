﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS_trial
{
    public partial class Page : System.Web.UI.Page
    {
        public string pageid
        {
            get
            {
                return Request.QueryString["page_id"];
            }
        }

        private string queryline = "SELECT * FROM MyPages WHERE page_id=";

        protected void Page_Load(object sender, EventArgs e)
        {
            queryline += pageid;
            page1_select.SelectCommand = queryline;
            Pages_Display(page1_select);
        }
        protected void Pages_Display(SqlDataSource src)
        {

            DataTable mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            foreach (DataRow row in mytbl.Rows)
            {
                main_h.InnerHtml = row["page_title"].ToString();
                main_cont.InnerHtml = row["page_content"].ToString();
                main_author.InnerHtml = row["page_author_fName"].ToString() +" " + row["page_author_lName"].ToString();
            }
        }

        protected void DeletePage(object sender, EventArgs e)
        {
            string delquery = "DELETE FROM MyPages WHERE page_id=" + pageid;

            del_page.DeleteCommand = delquery;
            del_page.Delete();
        }

    }
}
//the code contains adopted and modified parts from inclass example 